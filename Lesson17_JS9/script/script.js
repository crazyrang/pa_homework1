const field = document.querySelector(".field");
const ball = document.querySelector(".ball");
const scoreLeft = document.querySelector(".score-left");
const scoreRight = document.querySelector(".score-right");
const gateLeft = document.querySelector("#left-gate");
const gateRight = document.querySelector("#right-gate");
let goalLeft = 0;
let goalRight = 0;

field.addEventListener("dragover", preventEvent);
gateLeft.addEventListener("dragover", preventEvent);
gateRight.addEventListener("dragover", preventEvent);

function preventEvent(event) {
  event.preventDefault();
}

field.addEventListener("drop", dropBall);
gateLeft.addEventListener("drop", dropBall);
gateRight.addEventListener("drop", dropBall);

function dropBall(event) {
  this.append(ball);
  ball.style.left = event.offsetX - ball.offsetWidth / 2 + "px";
  ball.style.top = event.offsetY - ball.offsetHeight / 2 + "px";
  console.log(event.target.id);
  if (event.target.id == "left-gate") {
    goalLeft++;
    scoreLeft.innerHTML = goalLeft;
    console.log("left");
  }
  if (event.target.id == "right-gate") {
    goalRight++;
    scoreRight.innerHTML = goalRight;
    console.log("right");
  }
}
