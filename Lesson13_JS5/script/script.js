var navigation = document.querySelectorAll(".btn");
var content = document.querySelectorAll(".content");

function hideText() {
  for (var i = 0; i < navigation.length; i++) {
    content[i].classList.add("hide");
    navigation[i].classList.remove("pressed");
  }
}

for (let i = 0; i < navigation.length; i++) {
  navigation[i].addEventListener("click", showText);

  function showText() {
    hideText();
    content[i].classList.remove("hide");
    navigation[i].classList.add("pressed");
  }
}
